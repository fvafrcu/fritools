#/bin/sh
name=${1##R/}
function=${name%%.R}
cat > inst/runit_tests/runit-${name} <<EOF
if (interactive()) pkgload::load_all(".")
test_${function} <- function() {
    result <- ${function}(XXX)
    expectation <- XXX
    RUnit::checkIdentical(result, expectation)

}
if (interactive()) {
    test_${function}()
}
EOF
gvim -p R/${name} inst/runit_tests/runit-${name}
gvim log/covr.log
