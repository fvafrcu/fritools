Dear CRAN Team,
this is the initial submission of package 'fritools'.
It is a set helper functions and utilities for the Forest Research Institute of
the State Baden-Wuerttemberg that have been used for years.

It is a continuation of package `fritools`, which was archived on CRAN on
2022-09-04 due to a bug in a test suite introduced on 2022-08-16 in version 3.7.0.
It has been assigned to a Uwe Ligges for further inspection ever since.

Please upload to CRAN.
Best, Andreas Dominik

# Package fritools 4.1.0

Reporting is done by packager version 1.15.0


## Test environments
- R Under development (unstable) (2023-06-03 r84490)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- rhub
   ── fritools 4.1.0: OK
   
     Build ID:   fritools2_4.1.0.tar.gz-38269be3577d42b0867ccd82061f220c
     Platform:   Debian Linux, R-release, GCC
     Submitted:  3h 23m 57.4s ago
     Build time: 29m 23.3s
   
   0 errors ✔ | 0 warnings ✔ | 0 notes ✔
   [1] TRUE
- win-builder (devel)  
  Date: Mon, 05 Jun 2023 07:06:43 +0200
  Your package fritools2_4.1.0.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/X3Hzr7gBE2Nb
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 8
  Check time in seconds: 151
  Status: 1 NOTE
  R Under development (unstable) (2023-06-03 r84490 ucrt)
   

## Local test results
- RUnit:
    fritools_unit_test - 68 test functions, 0 errors, 0 failures in 251 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 4 ]
- tinytest:
     The results from these tests are not collected. Found the following occurrences:,  Remove the 'tinytest::' prefix to register the test results.FALSE, All ok, 241 results (19.9s)
- Coverage by covr:
    fritools Coverage: 75.36%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for tapply by 13.
     Exceeding maximum cyclomatic complexity of 10 for get_lines_between_tags by 9.
     Exceeding maximum cyclomatic complexity of 10 for is_running_on_fvafrcu_machines by 8.
     Exceeding maximum cyclomatic complexity of 10 for write_csv by 5.
     Exceeding maximum cyclomatic complexity of 10 for bulk_read_csv by 2.
     Exceeding maximum cyclomatic complexity of 10 for read_csv by 2.
     Exceeding maximum cyclomatic complexity of 10 for find_files by 1.
     Exceeding maximum cyclomatic complexity of 10 for weighted_variance.numeric by 1.
- lintr:
    found 0 lints in 3359 lines of code (a ratio of 0).
- cleanr:
    found 9 dreadful things about your code.
- codetools::checkUsagePackage:
    found 33 issues.
- devtools::spell_check:
    found 8 unkown words.
