Dear CRAN Team,
this is a resubmission of package 'fritools'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Andreas Dominik

# Package fritools 4.4.0.9000

Reporting is done by packager version 1.15.2.9000


## Test environments
- R Under development (unstable) (2025-01-07 r87537)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 5 (daedalus)
   0 errors | 0 warnings | 2 notes
- win-builder (devel)

## Local test results
- RUnit:
    fritools_unit_test - 68 test functions, 0 errors, 0 failures in 251 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 4 ]
- tinytest:
     The results from these tests are not collected. Found the following occurrences:,  Remove the 'tinytest::' prefix to register the test results.FALSE, All ok, 268 results (23.5s)
- Coverage by covr:
    fritools Coverage: 76.79%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for tapply by 13.
     Exceeding maximum cyclomatic complexity of 10 for get_lines_between_tags by 9.
     Exceeding maximum cyclomatic complexity of 10 for is_running_on_fvafrcu_machines by 8.
     Exceeding maximum cyclomatic complexity of 10 for write_csv by 5.
     Exceeding maximum cyclomatic complexity of 10 for bulk_read_csv by 2.
     Exceeding maximum cyclomatic complexity of 10 for read_csv by 2.
     Exceeding maximum cyclomatic complexity of 10 for find_files by 1.
     Exceeding maximum cyclomatic complexity of 10 for weighted_variance.numeric by 1.
- lintr:
    found 107 lints in 3594 lines of code (a ratio of 0.0298).
- cleanr:
    found 19 dreadful things about your code.
- codetools::checkUsagePackage:
    found 32 issues.
- devtools::spell_check:
    found 16 unkown words.
